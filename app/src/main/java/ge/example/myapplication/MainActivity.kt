package ge.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNaView = findViewById<BottomNavigationView>(R.id.bottomNavView)

        val navHostFragment = findNavController(R.id.nav_host_fragment)


        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.homefragment,
                R.id.dashboardfragment,
                R.id.notificationfragment,
                R.id.settingfragment
            )
        )


        setupActionBarWithNavController(navHostFragment, appBarConfiguration)
        bottomNaView.setupWithNavController(navHostFragment)
    }
}